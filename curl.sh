#! /bin/sh
# /*@@
#  @file   curl.sh
#  @date   2009-02-05
#  @author Erik Schnetter <schnetter@cct.lsu.edu.de>
#  @desc
#          Setup curl as a thorn
#  @enddesc
# @@*/



# /*@@
#   @routine    CCTK_Search
#   @date       Wed Jul 21 11:16:35 1999
#   @author     Tom Goodale
#   @desc
#   Used to search for something in various directories
#   @enddesc
#@@*/

CCTK_Search()
{
  eval  $1=""
  if test $# -lt 4 ; then
    cctk_basedir=""
  else
    cctk_basedir="$4/"
  fi
  for cctk_place in $2
    do
      if test -r "$cctk_basedir$cctk_place/$3" ; then
        eval $1="$cctk_place"
        break
      fi
      if test -d "$cctk_basedir$cctk_place/$3" ; then
        eval $1="$cctk_place"
        break
      fi
    done

  return
}


# Work out where curl is installed
if [ -z "$CURL_DIR" ]; then
  echo "BEGIN MESSAGE"
  echo 'CURL selected but no CURL_DIR set.  Checking some places...'
  echo "END MESSAGE"

  CCTK_Search CURL_DIR "/usr /usr/local /usr/local/curl /usr/local/packages/curl /usr/local/apps/curl $HOME c:/packages/curl" bin/curl-config
  if [ -z "$CURL_DIR" ]; then
     echo "BEGIN ERROR" 
     echo 'Unable to locate the curl directory -- please set CURL_DIR'
     echo "END ERROR" 
     exit 2
  fi
  echo "BEGIN MESSAGE"
  echo "Found a curl package in $CURL_DIR"
  echo "END MESSAGE"
fi



CURL_INC_DIRS="$(${CURL_DIR}/bin/curl-config --cflags | sed -e 's/^-I//g;s/ -I/ /g')"
CURL_LIB_DIRS="$(${CURL_DIR}/bin/curl-config --libs   | sed -e 's/^-L//g;s/ -L/ /g')"
CURL_LIBS=''

# Pass options to Cactus
echo "BEGIN MAKE_DEFINITION"
echo "HAVE_CURL     = 1"
echo "CURL_DIR      = ${CURL_DIR}"
echo "CURL_INC_DIRS = ${CURL_INC_DIRS}"
echo "CURL_LIB_DIRS = ${CURL_LIB_DIRS}"
echo "CURL_LIBS     = ${CURL_LIBS}"
echo "END MAKE_DEFINITION"

echo 'INCLUDE_DIRECTORY $(CURL_INC_DIRS)'
echo 'LIBRARY_DIRECTORY $(CURL_LIB_DIRS)'
echo 'LIBRARY           $(CURL_LIBS)'
